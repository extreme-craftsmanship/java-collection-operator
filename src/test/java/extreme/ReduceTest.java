package extreme;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class ReduceTest {

    @Test
    public void should_get_maximum_of_list() {
        // 获取数组中的最大值
        Integer[] array = new Integer[]{1, 5, 7, 2, 8, 9, 3, 2};
        List<Integer> arrayList = Arrays.asList(array);

        Reduce reduce = new Reduce(arrayList);

        assertEquals(9, reduce.getMaximum());
    }

    @Test
    public void should_get_minimum_of_list() {
        //获取数组中的最小值
        Integer[] array = new Integer[]{9, 4, 5, 10, 34, 2, 1, 10, 16};
        List<Integer> arrayList = Arrays.asList(array);

        Reduce reduce = new Reduce(arrayList);

        assertEquals(1, reduce.getMinimum());
    }

    @Test
    public void should_get_average_of_list() {
        // 获取数组的平均值
        Integer[] array = new Integer[]{12, 34, 56, 78, 90, 21};
        List<Integer> arrayList = Arrays.asList(array);

        Reduce reduce = new Reduce(arrayList);

        assertEquals(48.5, reduce.getAverage());
    }

    @Test
    public void should_get_element_in_middle_position_with_order_elements() {
        // 获取数组中位数
        Integer[] array = new Integer[]{1, 1, 1, 2, 3};
        List<Integer> arrayList = Arrays.asList(array);
        Reduce reduce = new Reduce(arrayList);

        assertEquals(1, reduce.getOrderedMedian());

        Integer[] evenArray = new Integer[]{1, 1, 2, 3};
        List<Integer> EvenArrayList = Arrays.asList(evenArray);
        Reduce evenReduce = new Reduce(EvenArrayList);

        assertEquals(1.5, reduce.getOrderedMedian());
    }

    @Test
    public void should_get_element_in_middle_position_in_linkList_when_even_size() {
        Integer[] array = new Integer[]{1, 4, 6, 2, 3, 10, 9, 8, 11, 2, 19, 30};
        List<Integer> arrayList = Arrays.asList(array);

        SingleLink<Integer> singleLink = mock(SingleLink.class);
        when(singleLink.getNode(6)).thenReturn(10);
        when(singleLink.getNode(7)).thenReturn(9);

        Reduce reduce = new Reduce(arrayList);

        assertEquals(9.5, reduce.getMedianInLinkList(singleLink));
    }

    @Test
    public void should_get_element_in_middle_position_in_linkList_when_odd_size() {
        Integer[] array = new Integer[]{1, 4, 6, 2, 3, 10, 9, 8, 11, 2, 19};
        List<Integer> arrayList = Arrays.asList(array);

        SingleLink<Integer> singleLink = mock(SingleLink.class);
        when(singleLink.getNode(6)).thenReturn(10);

        Reduce reduce = new Reduce(arrayList);

        assertEquals(10, reduce.getMedianInLinkList(singleLink));
    }

    @Test
    public void should_return_first_even_element() {
        //获取数组中第一个偶数
        Integer[] array = new Integer[]{1, 11, 27, 20, 4, 9, 15};
        List<Integer> arrayList = Arrays.asList(array);

        Reduce reduce = new Reduce(arrayList);

        assertEquals(20, reduce.getFirstEven());
    }

    @Test
    public void should_return_index_of_first_even_element() {
        //获取数组中第一个偶数的下标
        Integer[] array = new Integer[]{1, 11, 27, 20, 4, 9, 15, 4, 1, 11};
        List<Integer> arrayList = Arrays.asList(array);

        Reduce reduce = new Reduce(arrayList);

        assertEquals(3, reduce.getIndexOfFirstEven());
    }

    @Test
    public void should_return_last_even_element() {
        //获取数组中最后一个奇数
        Integer[] array = new Integer[]{1, 11, 27, 20, 4, 9, 15};
        List<Integer> arrayList = Arrays.asList(array);

        Reduce reduce = new Reduce(arrayList);

        assertEquals(15, reduce.getLastOdd());
    }

    @Test
    public void should_return_index_of_last_even_element() {
        //获取数组中最后一个奇数的下标
        Integer[] array = new Integer[]{1, 4, 27, 20, 4, 9, 15, 4, 1, 11};
        List<Integer> arrayList = Arrays.asList(array);

        Reduce reduce = new Reduce(arrayList);

        assertEquals(9, reduce.getIndexOfLastOdd());
    }

    @Test
    public void can_judge_whether_is_equal() {
        //判断两个数组是否相等
        Integer[] array = new Integer[]{1, 4, 27, 20, 4, 9, 15, 4, 1, 11};
        List<Integer> arrayList = Arrays.asList(array);

        Integer[] differentArray = new Integer[]{1, 4, 27, 20, 4, 9, 15, 4, 1};
        List<Integer> differentArrayList = Arrays.asList(differentArray);

        Reduce reduce = new Reduce(arrayList);

        assertTrue(reduce.isEqual(arrayList));
        assertFalse(reduce.isEqual(differentArrayList));
    }
}
