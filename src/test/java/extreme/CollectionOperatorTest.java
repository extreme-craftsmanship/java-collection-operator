package extreme;


import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class CollectionOperatorTest {

    @Test
    public void should_return_list_by_Interval() {
        //选出给定区间中所有的数字

        int right = 5;
        int left = 1;

        CollectionOperator collectionOperator = new CollectionOperator();

        Integer[] result = new Integer[]{1, 2, 3, 4, 5};
        List<Integer> resultList = Arrays.asList(result);

        assertEquals(resultList, collectionOperator.getListByInterval(left, right));

        Integer[] inverseResult = new Integer[]{5, 4, 3, 2, 1};
        List<Integer> inverseResultList = Arrays.asList(inverseResult);

        assertEquals(inverseResultList, collectionOperator.getListByInterval(right, left));
    }

    @Test
    public void should_return_list_by__two_Intervals() {
        //选出给定区间中所有的偶数
        int right = 10;
        int left = 1;

        CollectionOperator collectionOperator = new CollectionOperator();

        Integer[] result = new Integer[]{2, 4, 6, 8, 10};
        List<Integer> resultList = Arrays.asList(result);
        assertEquals(resultList, collectionOperator.getEvenListByIntervals(left, right));

        Integer[] inverseResult = new Integer[]{10, 8, 6, 4, 2};
        List<Integer> inverseResultList = Arrays.asList(inverseResult);
        assertEquals(inverseResultList, collectionOperator.getEvenListByIntervals(right, left));

    }

    @Test
    public void should_pop_even_elements() {
    //选出给定区间中所有的偶数
        int[] array = new int[]{1, 2, 3, 4, 5};

        Integer[] result = new Integer[]{2, 4};
        List<Integer> resultList = Arrays.asList(result);

        CollectionOperator collectionOperator = new CollectionOperator();

        assertEquals(resultList, collectionOperator.popEvenElements(array));
    }

    @Test
    public void should_pop_last_element() {
        //弹出集合最后一个元素
        int[] array = new int[]{1, 2, 3, 4, 5};

        CollectionOperator collectionOperator = new CollectionOperator();

        assertEquals(5, collectionOperator.popLastElement(array));
    }

    @Test
    public void should_pop_common_elements() {
        //弹出两个集合的交集
        int[] firstArray = new int[]{1, 2, 4, 6, 10};
        int[] secondArray = new int[]{3, 2, 6, 10, 8};

        Integer[] result = new Integer[]{2, 6, 10};
        List<Integer> resultList = Arrays.asList(result);

        CollectionOperator collectionOperator = new CollectionOperator();
        assertEquals(resultList, collectionOperator.popCommonElement(firstArray, secondArray));
    }

    @Test
    public void should_add_uncommon_elements_to_first_array() {
        // 将集合二中与集合一不同的元素加入集合一
        Integer[] firstArray = new Integer[]{1, 2, 4, 6, 10};
        Integer[] secondArray = new Integer[]{3, 2, 6, 10, 8};

        Integer[] result = new Integer[]{1, 2, 4, 6, 10, 3, 8};
        List<Integer> resultList = Arrays.asList(result);

        CollectionOperator collectionOperator = new CollectionOperator();
        assertEquals(7, collectionOperator.addUncommonElement(firstArray, secondArray).size());
        assertEquals(resultList, collectionOperator.addUncommonElement(firstArray, secondArray));
    }
}
